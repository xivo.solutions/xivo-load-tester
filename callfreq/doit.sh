#/bin/sh

factor="$1"; shift
port="$2"; shift

if [[ ! "$factor" =~ ^[.0-9]*[0-9]+$ ]]; then
    echo """Invalid factor
Usage for demo: ./doit.sh 0.4
Usage for stress test: ./doit.sh 0.8"""
    exit 1
fi

while :
do
	a=`date +%H%M`
	printf "$a "
	daterate=`grep $a rates |  awk -F ";" '{print $3}'`
 	if [ -n "$daterate" ]
	then
        	rate=`echo "$daterate * $factor" | bc`
        	cmd="cset rate $rate"
		echo "for $a : $daterate * $factor -> $cmd"
		echo $cmd | nc -u -q1 127.0.0.1 $port
	fi
sleep 10
done
