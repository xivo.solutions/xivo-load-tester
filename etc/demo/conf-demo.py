# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from loadtester.lines import generate_secure_lines
import os

etc_demo_folder = os.path.abspath('etc/demo')
export_csv = etc_demo_folder + '/user_export.csv'

## global configuration

#sipp_remote_host = '192.168.226.234'
sipp_remote_host_internal = '10.35.2.100'

sipp_local_ip = '10.35.2.103'
# sipp_call_rate = 6.0
# sipp_rate_period_in_ms = 60000
# sipp_max_simult_calls = 500
#sipp_nb_of_calls_before_exit = 10
#sipp_background = False
sipp_enable_trace_calldebug = True
sipp_enable_trace_err = True
#sipp_enable_trace_shortmsg = True
#sipp_enable_trace_stat = True


## global scenarios configuration

calling_line_internal = {
    'username': 'internaltests',
    'password': 'internaltests',
}
meanpause =  {
        'distribution': 'normal',
        'mean': 210000,
        'stdev': 90000,
}

codec = {
        'name': 'alaw',
        'pt': 8,
        'rtpmap': '8 PCMA/8000',
}

answerimmediate = {
   'distribution' : 'fixed',
   'value' : 10
}

rtp = 'test3s'

## scenarios configuration

scenarios.queues_low_rate = dict(
     sipp_call_rate = 1,
     sipp_rate_period_in_ms = 10000,
     bind_port = 5086,
     calling_line = calling_line_internal,
     called_extens = ['3005'],
     talk_time = meanpause,
     codec = codec,
)

scenarios.queues_medium_rate = dict(
    sipp_call_rate = 1,
    sipp_rate_period_in_ms = 6000,
    bind_port = 5087,
    calling_line = calling_line_internal,
    called_extens = ['3006'],
    talk_time = meanpause,
    codec = codec,
)

scenarios.queues_high_rate = dict(
    sipp_call_rate = 1,
    sipp_rate_period_in_ms = 2500,
    bind_port = 5088,
    calling_line = calling_line_internal,
    called_extens = ['3007'],
    talk_time = meanpause,
    codec = codec,
)

scenarios.register_sipp_agents = dict(
    bind_port = 5400,
    register_port = 5405,
    sipp_rate_period_in_ms = 60000, # 1 minute
    expires = 120, # register_expires
    sipp_call_rate = 6000 / 120,
    lines = generate_secure_lines(export_csv_from_xivo=export_csv, labels=['Surcharge', 'Moyen', 'Tranquille']),
)

scenarios.answer_then_hangup = dict(
    bind_port = 5405,
    ring_time = answerimmediate,
    rtp = rtp,
)