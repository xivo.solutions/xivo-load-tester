# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from loadtester.lines import generate_secure_lines
import os

## test data

test_data_folder = os.path.abspath('test_data')
register_internal = test_data_folder + '/load_send_sip_register_mds0_lines.csv'

## global configuration
mds0_host = os.getenv('mds0_host')
mds1_host = os.getenv('mds1_host')
sipp_remote_host = mds0_host

sipp_remote_host = '192.168.137.111'
mds0_host = '192.168.137.111'
mds1_host = '192.168.137.112'
sipp_remote_host_internal = mds0_host

sipp_local_ip = os.getenv('sipp_local_ip')
# sipp_local_ip = '10.181.23.16'
sipp_rate_period_in_ms = 60000
sipp_max_simult_calls = 10
register_expires = 120
register_rate = sipp_rate_period_in_ms / register_expires
sipp_enable_trace_stat = True
sipp_trace_rate=1

## global scenarios configuration

fake_gateway_line = {
    'username': 'fake-gateway',
    'password': 'fake-gateway',
}
calling_line = {
    'username': 'loadtester-latest',
    'password': 'loadtester-latest',
}
calling_line_from_extern = {
    'username': 'loadtester-from-extern',
    'password': 'loadtester-from-extern',
}
calling_to_webrtc_queue = {
    'username': 'loadtester-from-extern-to-webrtc',
    'password': 'loadtester-from-extern-to-webrtc',
}
meanpause =  {
   'distribution' : 'fixed',
   'value' : 1667000
}
answertalkpause = { 
   'distribution': 'uniform',
    'min': 36000,
    'max': 100000,
}
answerimmediate = {
   'distribution' : 'fixed',
   'value' : 10
}
codec = {
    'name': 'ulaw',
    'pt': 0,
    'rtpmap': '0 PCMU/8000',
}
codec_alaw = {
    'name': 'alaw',
    'pt': 8,
    'rtpmap': '8 PCMA/8000',
}

rtp = 'test3s'


## scenarios configuration with same name as the scenario directory

scenarios.call_webrtc_queue = dict(
    bind_port = 5074,
    control_port = 9002,
    calling_line = calling_to_webrtc_queue,
    called_extens = map(str,list(range(2000, 2007))),
    talk_time = answertalkpause,
    sipp_max_simult_calls = 16,
    sipp_call_rate = 4.0,
    rtp = rtp,
)

scenarios.call_sipp_queue = dict(
    bind_port = 5075,
    control_port = 9003,
    calling_line = fake_gateway_line,
    called_extens = map(str,list(range(76011, 76061))),
    talk_time = meanpause,
    sipp_max_simult_calls = 500,
    sipp_call_rate = 18.0,
    rtp = rtp,
)

scenarios.call_internal = dict(
    bind_port = 5076,
    calling_line = calling_line,
    called_extens = range(10001, 10099),
    talk_time = meanpause,
    sipp_max_simult_calls = 250,
    sipp_call_rate = 60.0,
    rtp = rtp,
)

scenarios.register_internal = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5400,
    register_port = 5405,
    sipp_call_rate = register_rate,
    lines = generate_secure_lines(register_internal),
    expires = register_expires,
)

scenarios.answer_then_hangup = dict(
    bind_port = 5405,
    ring_time = answerimmediate,
    rtp = rtp,
)
