# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from loadtester.lines import generate_secure_lines
import os

## test data

test_data_folder = os.path.abspath('test_data')
register_mds0 = test_data_folder + '/xds_send_sip_register_mds0_lines.csv'
register_mds1 = test_data_folder + '/xds_send_sip_register_mds1_lines.csv'

## global configuration

mds0_host = '192.168.137.111'
mds1_host = '192.168.137.112'
sipp_local_ip = '192.168.137.102'
sipp_call_rate = 1.0
register_rate = 55.0
sipp_rate_period_in_ms = 60000
sipp_max_simult_calls = 30
#sipp_enable_trace_calldebug = True
#sipp_enable_trace_err = True
#sipp_enable_trace_counts = True
sipp_enable_trace_stat = True

## global scenarios configuration

calling_line_mds0 = {
    'username': 'loadtester-latest',
    'password': 'loadtester-latest',
}

calling_line_mds1 = {
    'username': 'loadtester-mds1',
    'password': 'loadtester',
}

time_ringing = {
    'distribution': 'uniform',
    'min': 500,
    'max': 3000,
}

time_call = {
    'distribution': 'fixed',
    'value': 60000,
}

time_before_caller_hangup = {
    'distribution': 'uniform',
    'min': 10000,
    'max': 30000,
}

codec = {
        'name': 'alaw',
        'pt': 8,
        'rtpmap': '8 PCMA/8000',
}

rtp = 'test3s'

## scenarios configuration

scenarios.answer_then_hangup_mds0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5170,
    ring_time = time_ringing,
    talk_time = time_call,
)

scenarios.answer_then_hangup_mds1 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5171,
    ring_time = time_ringing,
    talk_time = time_call,
)

scenarios.send_sip_register_mds0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5160,
    register_port = 5170,
    sipp_call_rate = register_rate,
    lines = generate_secure_lines(register_mds0),
    expires = 3600,
)

scenarios.send_sip_register_mds1 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5161,
    register_port = 5171,
    sipp_call_rate = register_rate,
    lines = generate_secure_lines(register_mds1),
    expires = 3600,
)

### user call

scenarios.call_then_wait_mds0_0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5190,
    calling_line = calling_line_mds0,
    calls = range(1, 10000),
    called_extens = range(4000, 4039),
    talk_time = time_before_caller_hangup,
)

scenarios.call_then_wait_mds0_1 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5200,
    calling_line = calling_line_mds0,
    calls = range(1, 10000),
    called_extens = range(5000, 5039),
    talk_time = time_before_caller_hangup,
)

scenarios.call_then_wait_mds1_1 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5191,
    calling_line = calling_line_mds1,
    calls = range(1, 10000),
    called_extens = range(5000, 5039),
    talk_time = time_before_caller_hangup,
)

scenarios.call_then_wait_mds1_0 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5201,
    calling_line = calling_line_mds1,
    calls = range(1, 10000),
    called_extens = range(4000, 4039),
    talk_time = time_before_caller_hangup,
)

### group call

scenarios.group_call_then_wait_mds0_0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5210,
    calling_line = calling_line_mds0,
    calls = range(1, 10000),
    called_extens = ['8050'],
    talk_time = time_before_caller_hangup,
)

scenarios.group_call_then_wait_mds1_0 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5221,
    calling_line = calling_line_mds1,
    calls = range(1, 10000),
    called_extens = ['8050'],
    talk_time = time_before_caller_hangup,
)

### queue call

scenarios.queue_call_then_wait_mds0_0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5230,
    calling_line = calling_line_mds0,
    calls = range(1, 10000),
    called_extens = ['3830'],
    talk_time = time_before_caller_hangup,
)

scenarios.queue_call_then_wait_mds1_0 = dict(
    sipp_remote_host = mds1_host,
    bind_port = 5241,
    calling_line = calling_line_mds1,
    calls = range(1, 10000),
    called_extens = ['3830'],
    talk_time = time_before_caller_hangup,
)
