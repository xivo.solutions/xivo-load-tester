# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from loadtester.lines import generate_secure_lines
import os

## test data

test_data_folder = os.path.abspath('test_data')
register_mds0 = test_data_folder + '/outcall_send_sip_register_mds0_lines.csv'
outgoing_call_mds0 = test_data_folder + '/outgoing_call_mds0.csv'

## global configuration

mds0_host = '192.168.137.111'
mds1_host = '192.168.137.112'
gw_host = '192.168.137.101'
sipp_local_ip = '192.168.137.102'
sipp_call_rate = 10.0
register_rate = 55.0
sipp_rate_period_in_ms = 60000
sipp_max_simult_calls = 30
#sipp_enable_trace_calldebug = True
#sipp_enable_trace_err = True
#sipp_enable_trace_counts = True
sipp_enable_trace_stat = True

## global scenarios configuration

calling_line_mds0 = {
    'username': 'loadtester',
    'password': 'loadtester',
}

calling_line_mds1 = {
    'username': 'loadtester-mds1',
    'password': 'loadtester',
}

answerimmediate = {
   'distribution' : 'fixed',
   'value' : 10
}

time_ringing = {
    'distribution': 'uniform',
    'min': 3000,
    'max': 5000,
}

time_call = {
    'distribution': 'fixed',
    'value': 60000,
}

time_before_caller_hangup = {
    'distribution': 'uniform',
    'min': 10000,
    'max': 30000,
}

codec = {
    'name': 'alaw',
    'pt': 8,
    'rtpmap': '8 PCMA/8000',
}

rtp = 'test3s'

## scenarios configuration

scenarios.answer_then_hangup = dict(
    sipp_remote_host = sipp_local_ip,
    bind_port = 5309,
    ring_time = answerimmediate,
    talk_time = time_call,
    rtp = rtp,
)

scenarios.send_sip_register_mds0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5300,
    register_port = 5309,
    sipp_call_rate = register_rate,
    lines = generate_secure_lines(register_mds0),
    expires = 3600,
)

scenarios.outgoing_call_mds0 = dict(
    sipp_remote_host = mds0_host,
    bind_port = 5310,
    lines = generate_secure_lines(outgoing_call_mds0),
    called_extens = range(9991000, 9991049),
    called_extens_count = 50,
    talk_time = time_before_caller_hangup,
    codec = codec,
    rtp = rtp,
)
