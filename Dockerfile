FROM ubuntu:latest as build

ARG SIPP_VERSION=3.7.1

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -y pkg-config dh-autoreconf ncurses-dev build-essential cmake libssl-dev libpcap-dev libncurses5-dev libsctp-dev lksctp-tools gsl-bin libgsl27 libgsl-dev
ADD https://github.com/SIPp/sipp/releases/download/v${SIPP_VERSION}/sipp-${SIPP_VERSION}.tar.gz /
RUN tar -xzf /sipp-${SIPP_VERSION}.tar.gz
WORKDIR /sipp-${SIPP_VERSION}
RUN cmake . -DUSE_SSL=1 -DUSE_SCTP=1 -DUSE_PCAP=1 -DUSE_GSL=1
RUN make all


FROM ubuntu:latest
RUN apt-get update && apt-get install -y --no-install-recommends libncursesw6 libpcap0.8 libsctp1 python3 python3-pip libgsl27 libgsl-dev

RUN apt-get update
COPY --from=build /sipp-3.7.1/sipp /usr/local/bin/

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt --break-system-packages

COPY . /code
RUN chmod +x load-tester
# Build stage
FROM ubuntu:latest as build

ARG SIPP_VERSION=3.7.1

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    pkg-config \
    dh-autoreconf \
    ncurses-dev \
    build-essential \
    cmake \
    libssl-dev \
    libpcap-dev \
    libncurses5-dev \
    libsctp-dev \
    lksctp-tools \
    gsl-bin \
    libgsl-dev

ADD https://github.com/SIPp/sipp/releases/download/v${SIPP_VERSION}/sipp-${SIPP_VERSION}.tar.gz /
RUN tar -xzf /sipp-${SIPP_VERSION}.tar.gz
WORKDIR /sipp-${SIPP_VERSION}
RUN cmake . -DUSE_SSL=1 -DUSE_SCTP=1 -DUSE_PCAP=1 -DUSE_GSL=1
RUN make all

# Final stage
FROM ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    libncursesw6 \
    libpcap0.8 \
    libsctp1 \
    python3 \
    python3-pip \
    gsl-bin \
    libgsl27 \
    libgsl-dev \
    && rm -rf /var/lib/apt/lists/*

# Copy the built SIPp binary and necessary libraries
COPY --from=build /sipp-3.7.1/sipp /usr/local/bin/
COPY --from=build /usr/lib/x86_64-linux-gnu/libgsl* /usr/lib/x86_64-linux-gnu/

# Set library path
ENV LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt --break-system-packages

COPY . /code
RUN chmod +x load-tester
