#!/bin/bash

# csv files are monitored by collectd tail_csv plugin

# get all scenario directories and omit any registration scenario
SCENARIOS=$(ls -d scenarios/*/*/ | grep -v "reg")

while true; do
  reset
  echo -e "\n$(date) - copying newest scenario_*_.csv to scenario_stat.csv for collectd"

  for scenario in $SCENARIOS; do
    newest_stat_file=$(ls -t $scenario/scenario_*_.csv 2>/dev/null | head -1)
    if [ "$newest_stat_file" != "" ]; then
      tr ";" "," < "$newest_stat_file" > $scenario/scenario_stat.csv
    fi
  done
  sleep 60
done
