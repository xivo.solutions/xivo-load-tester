#!/bin/sh

sipp -inf users.csv -sf scenario.xml -cp {{ control_port }} -p {{ bind_port }} {{ sipp_std_options }} {{ sipp_remote_host }}

