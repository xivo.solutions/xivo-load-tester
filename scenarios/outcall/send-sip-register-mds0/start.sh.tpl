#!/bin/sh

LOGFILE="keep-alive.log"
HOST={{ sipp_remote_host }}

if [ ! -f $LOGFILE ]; then
    touch $LOGFILE
fi

while true; do
    sipp -inf users.csv -sf scenario.xml -p {{ bind_port }} {{ sipp_std_options }} {{ sipp_remote_host }}
    echo Writing to $LOGFILE
    echo Stopped $(date), remote host: $HOST >> $LOGFILE
    echo Resuming in 30 s
    sleep 30
done
