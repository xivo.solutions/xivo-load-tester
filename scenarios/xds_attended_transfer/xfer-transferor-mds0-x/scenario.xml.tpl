<?xml version="1.0" encoding="ISO-8859-1" ?>
<scenario name="answer call then hangup">

<recv request="INVITE">
   <action>
   <ereg regexp=".*" search_in="hdr" header="From:" assign_to="from_number"/>
   <ereg regexp=".*" search_in="hdr" header="To:" assign_to="to_number"/>
   <ereg regexp=".*" search_in="hdr" header="Contact:" assign_to="contact"/>
   <ereg regexp=".*" search_in="hdr" header="Call-ID:" assign_to="id"/>
   <!-- first var of assign_to contains the whole match -->
   <ereg regexp="tag=(.*)" search_in="hdr" header="From:" assign_to="trash,tag"/>
   </action>
</recv>

<!-- since SIPp complains about not used variable reference the trash var -->
<Reference variables="trash"/>

<send>
  <![CDATA[
    SIP/2.0 180 Ringing
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Length: 0

  ]]>
</send>

{{ ring_time|sipp_pause }}

<send retrans="500">
  <![CDATA[
    SIP/2.0 200 OK
    [last_Via:]
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    [last_CSeq:]
    Contact: <sip:[local_ip]:[local_port]>
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendrecv

  ]]>
</send>

<recv request="ACK">
</recv>

{{ rtp|sipp_rtp(codec) }}

{{ talk_time|sipp_pause }}

<nop display="Put caller on hold">
</nop>

<send retrans="500">
  <![CDATA[
    INVITE [$contact] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To:[$from_number]
    From:[$to_number];tag=[$tag]
    Contact:[$to_number]
    Call-ID:[$id]
    CSeq: [cseq] INVITE
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655766 2353687638 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendonly

  ]]>
</send>

<recv response="100" optional="true">
</recv>

<recv response="200">
</recv>

<send>
  <![CDATA[
    ACK sip:[call_number]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    [last_To:];tag=[call_number]
    [last_From:]
    Call-ID:[$id]
    CSeq: [cseq] ACK
    Content-Length: 0

  ]]>
</send>

{{ dial_time|sipp_pause }}

<nop display="Start second call to destination number">
</nop>

<send retrans="500">
  <![CDATA[
    INVITE sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To: <sip:[field2]@[remote_ip]:[remote_port]>
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    Call-ID: 12345///[call_id]
    CSeq: [cseq] INVITE
    Contact: <sip:[field0]@[local_ip]:[local_port]>
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendrecv

  ]]>
</send>

<recv response="401" auth="true">
</recv>

<send>
  <![CDATA[
    ACK sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To: <sip:[field2]@[remote_ip]:[remote_port]>[peer_tag_param]
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    Call-ID: [call_id]
    CSeq: [cseq] ACK
    Content-Length: 0

  ]]>
</send>

<send retrans="500">
  <![CDATA[
    INVITE sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To: <sip:[field2]@[remote_ip]:[remote_port]>
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    Call-ID: 12345///[call_id]
    CSeq: [cseq] INVITE
    Contact: <sip:[field0]@[local_ip]:[local_port]>
    [field1]
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendrecv

  ]]>
</send>

<recv response="100" optional="true">
</recv>

<recv response="180">
</recv>

<recv response="183" optional="true">
</recv>

<recv response="200">
</recv>

<send>
  <![CDATA[
    ACK sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To: <sip:[field2]@[remote_ip]:[remote_port]>[peer_tag_param]
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    Call-ID: 12345///[call_id]
    CSeq: [cseq] ACK
    Content-Length: 0

  ]]>
</send>

{{ talk_time|sipp_pause }}

<nop display="Put destination on hold">
</nop>

<send retrans="500">
  <![CDATA[
    INVITE sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    To: <sip:[field2]@[remote_ip]:[remote_port]>
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    Call-ID: 12345///[call_id]
    CSeq: [cseq] INVITE
    Contact: <sip:[field0]@[local_ip]:[local_port]>
    [field1]
    Content-Type: application/sdp
    Content-Length: [len]

    v=0
    o=user1 53655765 2353687637 IN IP[local_ip_type] [local_ip]
    s=-
    c=IN IP[media_ip_type] [media_ip]
    t=0 0
    m=audio [media_port] RTP/AVP {{ codec['pt'] }}
    a=rtpmap:{{ codec['rtpmap'] }}
    a=ptime:20
    a=sendonly

  ]]>
</send>

<recv response="100" optional="true">
</recv>

<recv response="200">
</recv>

<send>
  <![CDATA[
    ACK sip:[call_number]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    CSeq: [cseq] ACK
    Content-Length: 0

  ]]>
</send>

<!-- Complete transfer -->

<nop display="Sending Refer to destination instead of caller...">
</nop>

<send retrans="500">
  <![CDATA[

    REFER sip:[field2]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    From: <sip:[field0]@[local_ip]:[local_port]>;tag=[call_number]
    To: <sip:[field2]@[remote_ip]:[remote_port]>[peer_tag_param]
    Call-ID: 12345///[call_id]
    CSeq: [cseq] REFER
    Contact: sip:[field0]@[local_ip]:[local_port]
    Max-Forwards: 70
    Accept-Language: fr-fr,fr;q=0.9,en;q=0.8
    Refer-To: sip:[field2]@[remote_ip]:[remote_port];user=phone
    Referred-By: sip:[field0]@[local_ip]:[local_port]
    Content-Length: 0

  ]]>

</send>

<recv response="202">
</recv>

<!-- We should receive two NOTIFYs from Asterisk. One will be a 180 ringing sipfrag -->
<!-- and the other will be a 200 OK sipfrag -->

<recv request="NOTIFY">
</recv>

<send>
  <![CDATA[
    SIP/2.0 200 OK
    [last_Via:]
    [last_From:]
    [last_To:]
    [last_Call-ID:]
    [last_CSeq:]
    [last_Event:]
    Contact: <sip:[local_ip]:[local_port];transport=[transport]>
    Content-Length: 0

  ]]>
</send>

<send>
  <![CDATA[
    BYE sip:[call_number]@[remote_ip]:[remote_port] SIP/2.0
    Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
    Max-Forwards: 70
    [last_To:];tag=[call_number]
    [last_From:]
    [last_Call-ID:]
    CSeq: [cseq] BYE
    Content-Length: 0

  ]]>
</send>

<recv response="200">
</recv>

</scenario>
