# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import csv
from typing import List, Dict, Optional
import logging

logger = logging.getLogger('loadtester.lines')


def generate_lines(first_username: int, number_of_lines: int) -> List[Dict[str, str]]:
    logger.info(f"Generating {number_of_lines} lines starting from username {first_username}")
    lines: List[Dict[str, str]] = []

    for username in range(first_username, first_username + number_of_lines):
        line: Dict[str, str] = {'username': str(username), 'password': str(username)}
        lines.append(line)
        
    logger.info(f"Finished generating {number_of_lines} lines")
    return lines

def generate_secure_lines(
    export_csv_from_xivo: str,
    labels: Optional[List[str]] = None,
    number_range: Optional[tuple[str, str]] = None
) -> List[Dict[str, str]]:
    lines: List[Dict[str, str]] = []

    with open(export_csv_from_xivo) as export_csv_from_xivo:
        export_csv = csv.DictReader(export_csv_from_xivo)
        for row in export_csv:
            if labels and 'labels' in row:
                row_labels = row['labels'].split(',')
                if not any(label in row_labels for label in labels):
                    continue
            
            if number_range:
                start, end = number_range
                if not (start <= row.get('exten', '') <= end):
                    continue

            line: Dict[str, str] = {
                'username': row['sip_username'],
                'password': row['sip_secret']
            }
            lines.append(line)

    return lines
