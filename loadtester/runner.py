# -*- coding: utf-8 -*-

# Copyright (C) 2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from loadtester.scenario import Scenario
from loadtester.config import ScenarioConfig
from typing import Dict, Union
import logging

logger = logging.getLogger('loadtester.runner')

class ScenarioRunner:
    """Class responsible for running load testing scenarios."""

    def start_scenario(self, scenario: 'Scenario', scenario_config: 'ScenarioConfig') -> None:
        """
        Starts the execution of a load testing scenario.

        Args:
        - scenario: An instance of the Scenario class representing the scenario to be executed.
        - scenario_config: An instance of ScenarioConfig containing configuration details.

        Steps:
        1. Retrieves context for the scenario from the scenario_config.
        2. Prepares the scenario for execution by generating necessary files/templates.
        3. Executes the scenario.
        """
        logger.info(f"Starting scenario '{scenario.name}'")
        context: Dict[str, Union[str, int, bool]] = scenario_config.get_context_for_scenario(scenario.name)
        scenario.prepare_run(context)
        scenario.run()
        logger.info(f"Finished scenario '{scenario.name}'")
