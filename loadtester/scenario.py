# -*- coding: utf-8 -*-

# Copyright (C) 2013 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import errno
import os
import shutil
import subprocess
from jinja2 import Environment, FileSystemLoader, StrictUndefined
from typing import Dict, Generator, Union 
import logging

logger = logging.getLogger('loadtester.scenario')

class Scenario:
    """Represents a load testing scenario."""

    def __init__(self, directory: str, run_directory: str = "") -> None:
        """Initialize the Scenario object."""
        if not run_directory:
            run_directory = directory

        self.directory: str = directory
        self._run_directory: str = run_directory
        self.name: str = self._extract_scenario_name()
        logger.info(f"Initialized scenario '{self.name}' with directory: {self.directory}")

    def _extract_scenario_name(self) -> str:
        """Extract the scenario name based on the path."""
        return os.path.basename(self.directory.rstrip("/"))

    def prepare_run(self, context: Dict[str, Union[str, int, bool]]) -> None:
        """Prepare the scenario for execution."""
        logger.info(f"Preparing run for scenario '{self.name}'")
        if self.directory != self._run_directory:
            self._create_run_directory()
            self._copy_files_into_run_directory()
        # self._create_user
        self._generate_templates(context)
        logger.info(f"Finished preparing run for scenario '{self.name}'")

    def _create_run_directory(self) -> None:
        """Create a directory for running the scenario."""
        try:
            os.mkdir(self._run_directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def _copy_files_into_run_directory(self) -> None:
        """Copy necessary files into the run directory."""
        for filename in os.listdir(self.directory):
            if filename.endswith('.csv'):
                continue

            src_filename = os.path.join(self.directory, filename)
            dst_filename = os.path.join(self._run_directory, filename)
            shutil.copyfile(src_filename, dst_filename)

    def _generate_templates(self, context: Dict[str, Union[str, int, bool]]) -> None:
        """Generate files from templates."""
        tpl_processor = _TemplatesProcessor(self._run_directory, context)
        tpl_processor.generate_files()

    def run(self) -> None:
        """Execute the scenario."""
        logger.info(f"Running scenario '{self.name}'")
        process = subprocess.Popen(['sh', 'start.sh'], cwd=self._run_directory)
        process.communicate()
        logger.info(f"Finished running scenario '{self.name}'")

class _TemplatesProcessor:
    """Process .tpl templates for generating files."""

    _TEMPLATE_SUFFIX: str = '.tpl'
    _TEMPLATE_SUFFIX_LENGTH: int = len(_TEMPLATE_SUFFIX)

    def __init__(self, directory: str, context: Dict[str, Union[str, int, bool]]) -> None:
        """Initialize the TemplatesProcessor."""
        self._directory: str = directory
        self._context: Dict[str, Union[str, int, bool]] = context
        self._environment: Environment = self._new_environment(directory)

    def _new_environment(self, directory: str) -> Environment:
        """Create a new Jinja2 environment."""
        env: Environment = Environment(loader=FileSystemLoader(directory), undefined=StrictUndefined)
        env.filters['sipp_pause'] = _sipp_pause_filter
        env.filters['sipp_rtp'] = _sipp_rtp_filter
        return env


    def generate_files(self) -> None:
        """Generate files from templates."""
        for tpl_filename in self._list_template_filenames():
            self._generate_file_from_template(tpl_filename)

    def _list_template_filenames(self) -> Generator[str, None, None]:
        """List template filenames."""
        for filename in os.listdir(self._directory):
            if self._is_template_filename(filename):
                yield filename

    def _is_template_filename(self, filename: str) -> bool:
        """Check if a filename is a template file."""
        return filename.endswith(self._TEMPLATE_SUFFIX)

    def _generate_file_from_template(self, tpl_filename: str) -> None:
        """Generate a file from a template."""
        filename = self._get_template_destination(tpl_filename)
        template = self._environment.get_template(tpl_filename)
        template.stream(self._context).dump(filename)

    def _get_template_destination(self, tpl_filename: str) -> str:
        """Get the destination path for the generated template file."""
        filename: str = tpl_filename[:-self._TEMPLATE_SUFFIX_LENGTH]
        return os.path.join(self._directory, filename)

def _sipp_pause_filter(pause_dict: Dict[str, str]) -> str:
    """Jinja2 filter for generating a pause element."""
    attributes = ' '.join('%s="%s"' % item for item in pause_dict.items())
    return '<pause %s />' % attributes

def _sipp_rtp_filter(filename: str, codec: Dict[str, str], display: str = '') -> str:
    """Jinja2 filter for generating an RTP stream action."""
    if not filename:
        return ''

    filename = os.path.abspath(os.path.join('audio', '%s.%s' % (filename, codec['name'])))

    if display != '':
        return '<nop display="%s"><action><exec rtp_stream="%s,-1,%s"/></action></nop>' % (display, filename, codec['pt'])
    else:
        return '<nop><action><exec rtp_stream="%s,-1,%s"/></action></nop>' % (filename, codec['pt'])
