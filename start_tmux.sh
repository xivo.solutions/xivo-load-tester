#!/bin/bash

function set_window() {
	tmux rename-window $1
	tmux send "$2" C-m
}

function create_first_window() {
	tmux new-session -d -s "$3"
	set_window "$1" "$2"
}

function create_next_window() {	
	tmux new-window	
	set_window "$1" "$2"
}

function create_last_window() {
	create_next_window "$1" "$2"
	tmux detach -s "$3"
}

function agent_session() {
	session_name="agent"
	project_path="$HOME/xivo-load-tester"
	conf_file="$project_path/etc/load/conf-load.py"
	scenario_path="$project_path/scenarios/load_agent"
	gatling_path="$project_path/../gatlingxuc"
	call_freq=0.8
	call_hangup="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-hangup-gateway"
	answer_hangup="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/answer-then-hangup"
	#answer_hold="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/answer-then-hold"
	call_freq="cd $project_path/callfreq;./doit.sh $call_freq"
	int_call="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/internal-call"
	ext_call="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/external-call"
	group_call="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/external-call-to-group"
	call_freq="cd $project_path/callfreq; ./doit.sh $call_freq"
	#ccagent="cd $gatling_path;./looptest.sh start_load_agent.sh 50"
	r0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/send-sip-register-mds0"
	r1="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/send-sip-register-mds1"

	create_first_window "call_hangup" "$call_hangup" $session_name
	#create_next_window "answer_hold" "$answer_hold" 
	create_next_window "answer_hangup" "$answer_hangup" 
	create_next_window "int_call" "$int_call"
	create_next_window "ext_call" "$ext_call"
	create_next_window "group_call" "$group_call"
	create_next_window "call_freq" "$call_freq"
	#create_next_window "ccaget" "$ccagent"
	create_next_window "r0" "$r0"
	create_last_window "r1" "$r1" "$session_name"	
}

function outcall_session() {
	session_name="outcall"
	project_path="$HOME/xivo-load-tester"
	conf_file="$project_path/etc/load/conf-load-outcall.py"
	scenario_path="$project_path/scenarios/outcall"

	r0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/send-sip-register-mds0"
	a="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/answer-then-hangup"
	c0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/outgoing-call-mds0"
	#agw="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-hangup-gateway"

	create_first_window "r0" "$r0" "$session_name"
	create_next_window "a" "$a"
	#create_next_window "c0" "$c0"
	#create_last_window "agw" "$agw" "$session_name"
	create_last_window "c0" "$c0" "$session_name"
}

function transfer_session() {
	session_name="transfer"
	project_path="$HOME/xivo-load-tester"
	conf_file="$project_path/etc/load/conf-load.py"
	scenario_path="$project_path/scenarios/load_transfer"
	gatling_path="$project_path/../gatlingxuc"
	high_level_results="$project_path/../daily-high-level-tests/results"	
	it_before_calls=25
	recording_compilation_delay=25
	config_compilation_delay=50

	#agent="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/xfer-agent"
	#destination="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/xfer-destination"
	#caller="echo Waiting $wait_before_calls s for gatling scenario; for i in \$(seq $wait_before_calls -1 1); do echo -n \$i,; read -s -n 1 -t 1; done; cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/xfer-caller"
	#ccagent="cd $gatling_path;./looptest.sh start_load_transfer.sh 2"
	recording="echo Waiting $recording_compilation_delay s; for i in \$(seq $recording_compilation_delay -1 1); do echo -n \$i,; read -s -n 1 -t 1; done; cd $gatling_path;./looptest.sh start_load_recording.sh 60"
	config="echo Waiting $config_compilation_delay s; for i in \$(seq $config_compilation_delay -1 1); do echo -n \$i,; read -s -n 1 -t 1; done; cd $gatling_path;./looptest.sh start_load_config.sh 60"
	#high_level="grc tail -n 0 -f $high_level_results/*.log"
	
	create_first_window "recording" "$recording" "$session_name"
	create_last_window "config" "$config" "$session_name"
}

function xds_session() {
	session_name="xds"
	project_path="$HOME/xivo-load-tester"
	conf_file="$project_path/etc/load/conf-load-xds.py"
	scenario_path="$project_path/scenarios/xds"

	r0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/send-sip-register-mds0"
	r1="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/send-sip-register-mds1"
	ah0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/answer-then-hangup-mds0"
	ah1="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/answer-then-hangup-mds1"
	c0_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-wait-mds0-0"
	c0_1="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-wait-mds0-1"
	c1_1="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-wait-mds1-1"
	c1_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/call-then-wait-mds1-0"
	g0_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/group-call-then-wait-mds0-0"
	g1_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/group-call-then-wait-mds1-0"
	q0_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/queue-call-then-wait-mds0-0"
	q1_0="cd $project_path; $project_path/load-tester -c $conf_file $scenario_path/queue-call-then-wait-mds1-0"
	logs="cd $project_path; ./merge-logs.sh"

	create_first_window "r0" "$r0" "$session_name"
	create_next_window "r1" "$r1"
	create_next_window "ah0" "$ah0"
	create_next_window "ah1" "$ah1"
	create_next_window "c0_0" "$c0_0"
	create_next_window "c0_1" "$c0_1"
	create_next_window "c1_1" "$c1_1"
	create_next_window "c1_0" "$c1_0"
	create_next_window "g0_0" "$g0_0"
	create_next_window "g1_0" "$g1_0"
	create_next_window "q0_0" "$q0_0"
	create_next_window "q1_0" "$q1_0"
	create_last_window "logs" "$logs" "$session_name"
}

agent_session
outcall_session
#transfer_session
xds_session
