# xivo-load-tester

A call generator to do simple load testing scenarios.

Based on sipp.

## Usage

Copy `etc/conf.py.sample` to `etc/conf.py` and edit the latter.

* Change `sipp_remote_host` to the tested machine IP
* Change `sipp_local_ip` to your IP
* Change `bind_port` from 5060 to a free port number on your PC

Run `./load-tester scenarios/your-scenario`

Or run `ttmux` to run multi-scenario. See the *ttmux* section.


## Unit tests

    pip install jinja2
    nosetests loadtester/tests


## Dependencies

* sipp with OpenSSL, RTP streaming and GSL support

  * see *Compiling sipp*

* jinja2

    `apt-get install python-jinja2`

* ttmux to run multiple scenarios at once


### Compiling sipp

This version of load-tester has been tested against sipp 3.5.1.

1. Download the latest sipp release from https://github.com/SIPp/sipp/releases

2. Extract it:

    `tar xvf sipp-3.5.1.tar.gz`
    `cd sipp-3.5.1`

3. Install the build dependencies:

    `apt-get install build-essential pkg-config libssl-dev libncurses5-dev libgsl0-dev`

4. Build it:

    `./configure --with-openssl --with-rtpstream --with-gsl`
    `make`

5. Install it:

    `make install`


### Ttmux

Install from https://github.com/sensorario/ttmux

    cd /opt
    sudo git clone https://github.com/sensorario/ttmux.git
    sudo ln -s /opt/ttmux/ttmux /usr/local/bin/ttmux

Edit ttmux binary, fix shebang line (`sed -i.bak "1 s/.*/\#\!\/usr\/bin\/bash/" /opt/ttmux/ttmux`)
And replace 'start=1' with 'start=0' to fix pane numbers (`sed -i 's/start=/start=0/g' /opt/ttmux/ttmux`)


### Prepare XiVO

* Add SIP trunk from *Services > IPBX > Trunk management > SIP Protocol*
    * Name, Authentication username, Password, Caller ID: loadtester
    * Call limit: Unlimited
    * Connection type: User
    * IP Addressing type: Dynamic
* Create internal context "loadtest" and include it in default context
* Have some users on you XiVO
* Add devices to them
* Associate devices with users
* Dispatch lines to media servers if necessary, e.g.: `update linefeatures set configregistrar = 'mds1' where number between '2010' and '2019';`


### Important note

Comments on scenario screen (`<nop display="..." />`) can be used with caution.

* Then can cause error that the following response is not recognized randomly (in 3 of 4 cases) !
* They must not be placed after optionally received message - scenario won't run

### automate-things notes

All you need to do is :

- clone the repo and `cd` in
- create test_data directory
- add to 'test_data/load_register_sipp_agents.csv' the export of user (do it with xivo webi)
- `cp test_data/load_register_sipp_agents.csv test_data/load_send_sip_register_mds0_lines.csv`
- Modify value in docker-compose.yaml
- Build and launch the container : `docker-compose up -d --build`

Run the scenario you want : 

`docker exec -it xivo-load-tester-xivo-load-1 sh -c "./load-tester -c etc/load/conf-load-sogetrel.py scenarios/sogetrel/call-internal"`
`docker exec -it xivo-load-tester-xivo-load-1 sh -c "./load-tester -c etc/load/conf-load-sogetrel.py scenarios/sogetrel/register-internal/"`
